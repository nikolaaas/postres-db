variable "ssh_key_pub" {
  type = string
}

variable "user" {
  type = string
}

variable "postgres_db_ip" {
  type = string
}


resource "local_file" "ansible-inventory" {
 content = templatefile("${path.module}/templates/hosts.tpl",
   {
  postgres_db_ip   = var.postgres_db_ip
   }
 )
 filename = "${path.module}/ansible/inventory/hosts.ini"
}

resource "local_file" "ansible-external-vars" {
 content = templatefile("${path.module}/templates/external_vars.tpl",
   {
  user = var.user
   }
 )
 filename = "${path.module}/ansible/playbooks/external_vars.yml"
}

resource "proxmox_vm_qemu" "postgres-db" {
  count                  = 1
  name                   = "postgres-db"
  target_node            = "pve"
  clone                  = "ubuntu-1804-cloudinit-template"
  os_type                = "cloud-init"
  cores                  = 1
  sockets                = "1"
  cpu                    = "host"
  memory                 = 8192
  balloon                = 2048
  scsihw                 = "virtio-scsi-pci"
  bootdisk               = "scsi0"
  define_connection_info = false

  disk {
    size     = "50G"
    type     = "scsi"
    storage  = "vm_lvm"
    iothread = 0
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes  = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${var.postgres_db_ip}/24,gw=192.168.0.1"
  ciuser    = var.user
  sshkeys   = var.ssh_key_pub

}

