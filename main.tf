locals {
  ssh_key_pub         = file("~/.ssh/id_rsa.pub") 
  ssh_key             = file("~/.ssh/id_rsa") 
  user                = "nikolas"
  postgres_db_ip         = "192.168.0.50"
}

module "vm_provision" {
  source = "./vm_provision"
  ssh_key_pub          = local.ssh_key_pub
  user                 = local.user
  postgres_db_ip       = local.postgres_db_ip
}

resource "null_resource" "ansible_setup" {
 provisioner "local-exec" {
### sleep for 2 minutes for the machine to cooldown and the vms to properly bootup
  command = "sleep 30 && ansible-playbook vm_provision/ansible/playbooks/setup.yml --user ${local.user} -i vm_provision/ansible/inventory/hosts.ini"
  }
 depends_on = [module.vm_provision]
}

