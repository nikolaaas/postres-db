# Postgres DB Using Terraform and Ansible

Simple deployment setup for a VM based Postgres DB deployment, using terraform and ansible on proxmox.


```bash
$ export PM_PASS="your_proxmox_password"
$ terraform init
$ terraform apply
```

## Requirements
in order to run this setup, one needs to have installed terraform and ansible and on the server side proxmox hypervisor.

